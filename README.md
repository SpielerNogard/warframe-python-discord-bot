# Warframe Python Discord BOT

The goal is to write a bot that shows all the important things about the game Warframe and contains a few minor player series

planned functions:
    
    - Eidolon Clock
    - Arbitration Tracker
    - Randomizer (Warframes, Weapons ...)
    - Drops ( best place to farm a item)
    - Damage Simulator
    - Riven Calculator
    - Riven Market
    - Marketplace
    - Challenges
    - News
    - Patches
    
    